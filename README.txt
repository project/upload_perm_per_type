
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Dave.Ingram <http://drupal.org/user/352282>

Upload Permissions Per Content Type is a simple module that provides control
over the file upload form per content type, per role.

For example, you can turn off file uploads on forum topics for authenticated
users, while leaving it enabled for site administrators, while still allowing
for all users to upload files to blog entries.


INSTALLATION
------------

1. Copy the files to your sites/SITENAME/modules directory.

2. Enable the module at Administer >> Site building >> Modules
   (admin/build/modules).

3. Customize the module settings at Administer >> Settings >> Upload permissions
   per content type (admin/settings/upload_perm_per_type). You may enable or
   disable upload permissions per content type, per role.

